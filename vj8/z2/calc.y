%{
#include <stdio.h>
#include <math.h>
%}
%left '+' '-'
%left '*' '/'
%left '^'
%token NUMBER
%%
statement: { /* empty line */ }
| statement expression '\n' {printf("%d\n", $2);}
;
expression: expression '+' expression { $$ = $1 + $3; }
| expression '-' expression { $$ = $1 - $3; }
| expression '*' expression { $$ = $1 * $3; }
| expression '/' expression { $$ = $1 / $3; }
| expression '^' expression { $$ = (int)((pow($1, $3) > 0.0) ? floor(pow($1, $3) + 0.5) : ceil(pow($1, $3) - 0.5)); }
| NUMBER { $$ = $1; }
;
%%
int main(void) { return yyparse(); }
int yyerror(char *msg) { printf("%s\n", msg); }
