%{
#include <stdio.h>
%}
%left '+' '-'
%left '*' '/'
%token NUMBER
%%
statement: { /* empty line */ }
| statement expression '\n' {printf("%d\n", $2);}
;
expression: expression '+' expression { $$ = $1 + $3; }
| expression '-' expression { $$ = $1 - $3; }
| expression '*' expression { $$ = $1 * $3; }
| expression '/' expression { $$ = $1 / $3; }
| NUMBER { $$ = $1; }
;
%%
int main(void) { return yyparse(); }
int yyerror(char *msg) { printf("%s\n", msg); }
