/*
Napiši lexer koji parsira jednostavne LEX definicijske
datoteke i ispisuje pojedine definicije, pravila i ostali kod sa
opisom. Koristiti stanja. BONUS: Rezultirajući program mora
parsirati vlastitu *.l definicijsku datoteku.
Primjer jednostavne ulazne datoteke i ispisa iz programa:


%{
#define TEST
%}
NUM [0-9]+
%%
{NUM} printf("Broj: %s", yytext);
%%
int main() { return 0; }
Pronađeno zaglavlje: #define TEST
Pronađena definicija imena NUM, za [0-9]+
Pronađeno pravilo za {NUM}, koda printf("Broj: %s", yytext);
Pronađen blok koda int main() { return 0; }
...

*/

%{

#include <string.h>
#include <stdlib.h>
int poz = 0;

%}

zaglavlje	^((%\{)|(%\}))\n$
sadrzaj	^[^(%\{)(%\})(%%)].*\n
pravila	^%%\n$

%%
{zaglavlje}	{
	switch(poz){
		case 0: 
			poz=1;
		break;
		case 1:
			poz=2;
		break;
	}
}
{sadrzaj}	{
	switch(poz){
		case 1:
			printf("Pronadjeno zaglavlje: %s\n", yytext);
		break;
		case 2:
			printf("Pronadjena definicija: %s\n", yytext);
		break;
		case 3:
			printf("Pronadjeno pravilo: %s\n", yytext);
		break;
		case 4:
			printf("Pronadjen blok koda: %s\n", yytext);
		break;
	}
}
{pravila} {
	switch(poz){
		case 2: poz=3; break;
		case 3: poz=4; break; 
	}
}


%%

main(){
yylex();
return 0;
}
