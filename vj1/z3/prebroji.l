/*
Prebrojavanje (UNIX wc)
*/

%{
unsigned int chrCount = 0, wordCount = 0, lineCount = 0;
%}

word	[^ \t\n]+
eol	\n

%%

{word}	{ wordCount++; chrCount += yyleng; }
{eol}	{ chrCount++; lineCount++; }
.	chrCount++;

%%

main()
{
yylex();
printf("Broj linija: %d\nBroj rijeci: %d\nBroj znakova: %d\n", lineCount, wordCount, chrCount);
}
