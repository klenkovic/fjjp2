%%
[\t]+	/*ignore blank*/;
sam|si|je|smo|ste|su	{ printf("%s: je glagol\n", yytext); }
Borna|Luka|Maja|Ivan|Ana|Sanda	{ printf("%s: je ime\n", yytext); }
student|studentica|ucitelj	{ printf("%s: je imenica\n", yytext); }
ja|ti|on|ona|mi|vi|oni|one	{ printf("%s: je zamjenica\n",
yytext); }
[a-zA-Z]+	{ printf("%s: ne prepoznajem\n", yytext); }
.|\n	{ ECHO; }
%%
main()
{
yylex();
}
