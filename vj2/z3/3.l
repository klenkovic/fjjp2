/*
Napiši lexer koji razlikuje ključne riječi, integer
vrijednosti, float vrijednosti (decimalni broj),
operatore i komentare u Pythonu. A sve ostale
znakove ispisuje "Ne prepoznajem znak".
- ključne riječi: if, else, elif, for, while, def, or, ...
- operatori: +, -, /, *
(preskočiti prazna mjesta i ":")
*/

%{

#include <string.h>



%}

intbr	-?[0-9]*
flbr	-?[0-9]*\.[0-9]*
klj	(if|else|elif|for|while|def|or)
ops	(>=)|(<=)|[+*-/<>=]
kom	#.*$

%%
{intbr}	{ printf("%s je cijeli broj.\n", yytext); }
{flbr}	{ printf("%s je decimalni broj.\n", yytext); }
{klj}	{ printf("%s je kljucna rijec.\n", yytext); }
{ops}	{ printf("%s je operator.\n", yytext); }
{kom}	{ printf("%s je komentar.\n", yytext);}
[\n\t :=]	;
.	{printf("Ne prepoznajem znak %s.\n", yytext);}

%%

main(){
yylex();
return 0;
}
