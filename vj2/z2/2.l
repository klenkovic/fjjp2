/*
Napiši lexer koji "cita" cijele brojeve. Korisnik
unosi dva broja i program vraca njihov zbroj.
Primjer:
5
10
Zbroj: 5+10=15
-2
5
Zbroj: -2+5=3
*/

%{

#include <stdlib.h>


int a,b,c;
int ucitanPrvi = 0;

%}

broj	[0-9]*

%%
{broj}	{
		if(ucitanPrvi==0){
			a = atoi(yytext);
			ucitanPrvi = 1;

		}else{
			b = atoi(yytext);
			c = a + b;
			printf("Zbroj: %d+%d=%d\n", a, b, c);
			ucitanPrvi = 0;
		}
	}
[\n]|.	;
%%

main(){
yylex();
}
