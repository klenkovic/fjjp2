/*
Napiši lexer koji nalazi najdulju riječ iz inputa.
Na kraju se ispisuje (popuniti linije):
Najduza rijec je ___, koja ima ___ znakova.


 flex 1.l
 gcc -o prvi lex.yy.cc -lfl ./prvi

 ./prvi < primjer.txt
 pozivanje datoteke
*/

%{

#include <string.h>

char rijec[255];
int br=0;

%}

rijec	[^ \t\n]+

%%
{rijec}	{
		if(yyleng>br){
			br=yyleng;
			strcpy(rijec, yytext);
		}
	}
[\n]|.	;
%%

main(){
yylex();
printf("Najduza rijec je %s, koja ima %d znakova.\n", rijec, br);
}
