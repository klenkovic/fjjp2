%{
#include <stdlib.h>
#include "complex.h"
#include "y.tab.h"
%}

%%

[0-9]*\.[0-9]+    { yylval.doubleValue = atof(yytext); return DOUBLE; }
[0-9]+    { yylval.intValue = atoi(yytext); return INTEGER; }
[ \t]     ;
\n|.      { return yytext[0]; }

%%
