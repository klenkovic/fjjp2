%{
#include <stdio.h>
#include "complex.h"
%}
%union {
	int intValue;
	double doubleValue;
	complex_t complexValue;
}
%token <intValue> INTEGER
%token <doubleValue> DOUBLE
%type <doubleValue> expression
%type <complexValue> complex_exp
%%
statement: 
         | statement expression '\n' {printf("%f\n", $2);}
         ;
expression: expression '+' expression { $$ = $1 + $3; }
          | INTEGER { $$ = $1; }
          | DOUBLE { $$ = $1; }
		  | complex_exp { printf("Kompleksni broj, real: %d, imag: %d\n", $1.real, $1.imag); }
          ;
complex_exp: INTEGER '+' 'i' INTEGER { $$.real = $1; $$.imag = $4; }
		   ;
%%
int main(void) { return yyparse(); }
int yyerror(char *msg) { printf("%s\n", msg); }
