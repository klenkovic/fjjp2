/*

Dodati reprezentaciju kompleksnih brojeva.
(Primjer: 10+j12, 0-j5)
Kompleksni broj predstaviti sa C struct.
Lex vraća parsirani kompleksni broj kao yylval Yacc-u.

*/

%{
#include <stdlib.h>
#include <string.h>
#include "y.tab.h"
%}
%%
[0-9]+[+-]i[0-9]+	{sscanf(yytext, "%d%[+-]i%d", &(yylval.cVal.real), &(yylval.cVal.imag)); return COMPLEX;}
[0-9]*\.[0-9]+	{yylval.doubleValue = atof(yytext); return DOUBLE;}
[0-9]+	{yylval.intValue = atoi(yytext); return INTEGER;}
[ \t]	;
\n|.	{ return yytext[0]; }
%%
