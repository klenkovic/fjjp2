/*

Dodati reprezentaciju kompleksnih brojeva.
(Primjer: 10+j12, 0-j5)
Kompleksni broj predstaviti sa C struct.
Lex vraća parsirani kompleksni broj kao yylval Yacc-u.

*/

%{
#include <stdio.h>

typedef struct {
	int real;
	int imag;
} complex_t;

%}
%union {
	int intValue;
	double doubleValue;
	complex_t cVal;
}
%token <intValue> INTEGER
%token <doubleValue> DOUBLE
%token <cVal> COMPLEX
%type <doubleValue> expression
%%
statement:
| statement expression '\n' {printf("%f\n", $2);}
;
expression: expression '+' expression { $$ = $1 + $3; }
| INTEGER { $$ = $1; }
| DOUBLE { $$ = $1; }
| COMPLEX { printf("c:%d %d\n", $1.real, $1.imag); $$ = 0; }
;
%%
int main(void) { return yyparse(); }
int yyerror(char *msg) { printf("%s\n", msg); }
