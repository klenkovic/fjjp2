%{
#include <stdio.h>
%}
%union {
int intValue;
double doubleValue;
}
%token <intValue> INTEGER
%token <doubleValue> DOUBLE
%type <doubleValue> expression
%%
statement:
| statement expression '\n' {printf("%f\n", $2);}
;
expression: expression '+' expression { $$ = $1 + $3; }
| INTEGER { $$ = $1; }
| DOUBLE { $$ = $1; }
;
%%
int main(void) { return yyparse(); }
int yyerror(char *msg) { printf("%s\n", msg); }
