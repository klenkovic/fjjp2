%{
#include <stdio.h>
%}
%token INTEGER
%%
statement:
| statement expression '\n' {printf("%d\n", $2);}
;
expression: expression '+' expression { $$ = $1 + $3; }
| INTEGER { $$ = $1; }
;
%%
int main(void) { return yyparse(); }
int yyerror(char *msg) { printf("%s\n", msg); }
